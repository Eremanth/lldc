# LLDC

## Installation on a Windows x64 computer

- Clone the repository

## Docker

- Install Docker
- Install Kitematic

### Initialize the containers

```bash
docker-compose build
docker-compose up -d
```

You're ready to go :
- backend : http://localhost:8000/public/
- frontend : http://localhost:4200

### Execute a commmand in the container

You can use the Exec button in Kitematic to execute command into a container.

Backend container :

```
cd api
composer install
php bin/console doctrine:database:create
```

Frontend container :

```
# I don't have any fucking clue
```

### Stop the containers

```bash
docker-compose kill
```

## Without Docker

- Install Nginx or Apache Server, with at least PHP 7.2.6
- Install NodeJs 8.11.3 or higher (npm 5.6.0)

For Windows, you might add PHP binaries path and NodeJS path to your OS environment variables.
